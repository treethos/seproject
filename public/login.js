import { initializeApp } from "https://www.gstatic.com/firebasejs/9.4.1/firebase-app.js";
import {
  getFirestore, //main
  collection, //sub-main
  getDocs, //recall data to show output
} from "https://www.gstatic.com/firebasejs/9.4.1/firebase-firestore-lite.js";
import {} from "https://www.gstatic.com/firebasejs/9.4.1/firebase-firestore.js";
import {
  getAuth, //main
  signInWithEmailAndPassword, //sub-main
  onAuthStateChanged,// stage after signin
} from "https://www.gstatic.com/firebasejs/9.4.1/firebase-auth.js";


const firebaseConfig ={
  apiKey: "AIzaSyDHanLtulMLBc2qM3KK9CDexN4iJMZ4bxw",
  authDomain: "se-project-3c7f0.firebaseapp.com",
  projectId: "se-project-3c7f0",
  storageBucket: "se-project-3c7f0.appspot.com",
  messagingSenderId: "792377212394",
  appId: "1:792377212394:web:6265ce6f5bad5e6e670693",
  measurementId: "G-N2RZ2DQM0Y"
};
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth();

const loginFrom = document.querySelector("#login-form");
loginFrom.addEventListener("submit", (e) => {
  e.preventDefault();
  const email = loginFrom["login-email"].value;
  const password = loginFrom["login-password"].value;
  signInWithEmailAndPassword(auth, email, password).then((userCredential) => {
    loginFrom.reset();
  });
});

onAuthStateChanged(auth, (user) => {
  if (user) {
    window.location = "home.html";
  }
});

