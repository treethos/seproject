import { initializeApp } from "https://www.gstatic.com/firebasejs/9.4.1/firebase-app.js";
import {
  getFirestore,
  collection,
  addDoc,
  doc,
  setDoc,
  Timestamp,
} from "https://www.gstatic.com/firebasejs/9.4.1/firebase-firestore.js";
import {
  getAuth, //main
  createUserWithEmailAndPassword,
  onAuthStateChanged, // stage after signin
} from "https://www.gstatic.com/firebasejs/9.4.1/firebase-auth.js";

const firebaseConfig ={
    apiKey: "AIzaSyDHanLtulMLBc2qM3KK9CDexN4iJMZ4bxw",
    authDomain: "se-project-3c7f0.firebaseapp.com",
    projectId: "se-project-3c7f0",
    storageBucket: "se-project-3c7f0.appspot.com",
    messagingSenderId: "792377212394",
    appId: "1:792377212394:web:6265ce6f5bad5e6e670693",
    measurementId: "G-N2RZ2DQM0Y"
  };

    const app = initializeApp(firebaseConfig);
    const db = getFirestore(app);
    
    const auth = getAuth();
    const singupFrom = document.querySelector("#signup-form");
    singupFrom.addEventListener("submit", (e) => {
      e.preventDefault();
      const email = singupFrom["signup-email"].value;
      const password = singupFrom["signup-password"].value;
      createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          setDoc(doc(db, userCredential.user.uid, "data"), {
            FirstName: singupFrom["signup-firstname"].value,
            LastName: singupFrom["signup-lastname"].value,
            Email: singupFrom["signup-email"].value,
            Password: singupFrom["signup-password"].value,
          });
        })
        .then(() => {
          console.log("Document successfully written!");
          singupFrom.reset();
        })
    });
    
    // onAuthStateChanged(auth, (user) => {
    //   if (user) {
    //     setTimeout(function () {
    //       window.location = "login.html";
    //     }, 2000);
    //     addingredients();
    //     addtype();
    //   }
    // });